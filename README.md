# @MFKip

### Packages

> in order of relevance

 - mfkip-web: A lib for using an MFKip stack to create p2p web applications
 - ipmfk-webrtc-signal: Our implementation of a signaling server that isolates peer discovery based on client specified 'partitions'
 - ipmfk-webrtc: Fork of `libp2p-webrtc-star`, used by ipmfk-webrtc-signal, necessary because we needed to pass additional params when connecting to the signaling server
 - [**unmaintained**] core: Not well maintained, was to be a common package available to be used in both browser and nodejs scopes
 - [**unmaintained**] web-node: example chat web application but completely out-of-date with changes to mfkip-web
 - [**unmaintained**] nodejs-node: example of using the MFKip stack in a p2p nodejs application

## TODO

 - provide multiple discovery servers, use case hosted and locally deployed instances potentially incorporated into the 'backbone'
 - 'backbone' desktop nodes that subscribe to all data and provide relays

 - can we put orbitDb onto a worker
 - stop connections when web page is background throttled
   - keep awake lock

## Takeaways:
 - segregate data, replication take longer the larger the db
    - 10000 msgs requires 70-80 MB to replicate ~5min to replicate, ~45s to cold load


# Idea: Backbone node roles

> The p2p concept works great for realtime collaboration where all clients interact simultaneously.  When clients begin contributing data at disparate times, p2p becomes less compelling.  This is a theorized 'backbone' node that would be a peer that remains always online, never contributing data, but always listening and collecting.  This allows the backbone to be a generic implementation that would provide data delivery guarantees even when p2p nodes contribute data without other peers present

 - Source for all information in its entirety (...or as close to that as possible)
    - relay between chokepoints
 - Randevous point to discover peers
    - enables Multiple bubbles topology
 - Allow more connections than web nodes
    - Stability
    - More data replication
 - Bootstrap node for web clients
 - Alternative discovery of other backbone nodes, i.e mdns
 - multiple backbones could serve as multiple fail-over points

