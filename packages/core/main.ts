import Libp2p from "libp2p";
import MPLEX from 'libp2p-mplex';
import { NOISE } from 'libp2p-noise';
import WebRTCStar from '@mfkip/webrtc';
import Protector from 'libp2p/src/pnet';
import uint8FromString from 'uint8arrays/from-string';
import { create as IPFSCreate } from 'ipfs';
import gossipsub from 'libp2p-gossipsub';

const isNode = typeof module !== 'undefined' && module.exports;

let libp2pNodeResolver: any;

export const libp2pNode = new Promise(res => libp2pNodeResolver = res);

export async function create({
    encodedKey,
    listenAddresses,
    minConnections,
    maxConnections,
    maxData,
    partitions = ['partition1'],
    wrtc,
    repo,
}: StartOptions) {
    const swarmKey = uint8FromString(Buffer.from(encodedKey, `base64`).toString(`binary`));

    const transportKey = WebRTCStar.prototype[Symbol.toStringTag]

    const libP2pConfig = (opts: any) => new Libp2p({
        peerId: opts.peerId,
        modules: <any>{
            transport: [WebRTCStar],
            streamMuxer: [MPLEX],
            connEncryption: [NOISE],
            connProtector: new Protector(swarmKey),
            pubsub: gossipsub,
        },
        connectionManager: {
            minConnections,
            maxConnections,
            maxData,
        },
        addresses: {
            listen: listenAddresses
        },
        config: {
            transport: {
                [transportKey]: {
                    listenerOptions: {
                        partitions
                    },
                    wrtc
                }
            }
        },
        metrics: <any>{
            enabled: true,
            computeThrottleMaxQueueSize: 1000,
            computeThrottleTimeout: 2000,
            movingAverageIntervals: [
                60 * 1000,
                5 * 60 * 1000,
                15 * 60 * 1000 // 15 minutes
            ],
            maxOldPeersRetention: 50
        }
    });

    const node = await IPFSCreate({
        start: false,
        preload: { enabled: false },
        libp2p: libP2pConfig,
        repo: repo || `${isNode ? '~/' : ''}${partitions.join('-')}`,
    });

    // console.log('Starting');
    // await node.start();
    // console.log('Started');

    // (node as any).libp2p.connectionManager.on('peer:connect', (conn: any) => {
    //     console.log('>>>>> Connected', conn.remotePeer.toB58String());
    // });
    // (node as any).libp2p.connectionManager.on('peer:disconnect', (conn: any) => {
    //     console.log('>>>>> Disconnected', conn.remotePeer.toB58String());
    // });

    libp2pNodeResolver(node);
    return node;
};

// export const orbitDbNode = (async () => {
//     return await OrbitDB.createInstance(await libp2pNode);
// })();

export interface StartOptions {
    encodedKey: string,
    listenAddresses: string[],
    minConnections?: number,
    maxConnections?: number,
    maxData?: number,
    partitions?: string[],
    wrtc?: any,
    repo?: string,
}
