const { fromString, toString } = require('uint8arrays');
const { randomBytes } = require('crypto');

const KEY_LENGTH = 32

const bytes = new Uint8Array(95);
const psk = toString(randomBytes(KEY_LENGTH), 'base16')
const key = fromString('/key/swarm/psk/1.0.0/\n/base16/\n' + psk)

bytes.set(key);

console.log(toString(bytes, 'base64')); 