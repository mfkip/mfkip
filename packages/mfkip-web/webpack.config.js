const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const mode = process.env.NODE_ENV || 'development';
const prod = mode === 'production';

module.exports = {
	entry: {
		index: ['./src/index.js'],
	},
	resolve: {
		extensions: ['.js']
	},
	output: {
		path: __dirname + '/dist',
		filename: '[name].js',
	},
	mode,
	devtool: prod ? false: 'source-map',
    plugins: [
        new CopyWebpackPlugin({
            patterns: [
                { from: './src/index.d.ts' },
				{ from: './src/orbit-db-types/**/*.d.ts', to({absoluteFilename}) {
					const path = absoluteFilename.split('src')[1].split('\\').slice(1, -1).join('/');
					return `${path}/[name].ts`;
				} }
            ]
        })
    ]
};
