import { create } from '../../core';
import * as OrbitDB from 'orbit-db';
import { fromEvent, defer, Subject, merge } from 'rxjs';
import { finalize, tap, switchMap, map, filter } from 'rxjs/operators';
import { fromString, toString } from 'uint8arrays';

const KEY_LENGTH = 32
const DATABASE_MODIFIERS = new Set(['put', 'set', 'del', 'add', 'remove', 'inc']);

globalThis.MFKIP = class MFKIP {

    static createSwarmKey() {
        const bytes = new Uint8Array(95);
        const psk = toString(window.crypto.getRandomValues(new Uint8Array(KEY_LENGTH)), 'base16')
        const key = fromString('/key/swarm/psk/1.0.0/\n/base16/\n' + psk)
        
        bytes.set(key);
        
        return toString(bytes, 'base64');
    }
    
    constructor(createOptions) {
        this.ipfsStarted = defered();
        this.ipfs = create({
            ...createOptions,
            minConnections: Math.max(createOptions.minConnections, 1),
            partitions: (createOptions.partitions || ['']).map(p => `${createOptions.appId}-${p}`),
        });

        this.orbitdb = this.ipfsStarted.promise.then(node => OrbitDB.createInstance(node));
        this.pubSub = new PubSubService(this.ipfsStarted.promise);
    }

    async start() {
        const ipfsNode = await this.ipfs;

        return ipfsNode.start().then(() => {
            this.ipfsStarted.resolver(ipfsNode);
            return ipfsNode;
        });
    }

    async openDb(name, type, options = {}) {
        if (!options.accessController) {
            options.accessController = { write: ['*'] };
        }

        const orbitDb = await this.orbitdb;
        const db = await orbitDb[type](name, options);

        db.events.on('replicated', () => db.events.emit('mfkip.change'));
        db.events.once('ready', () => db.events.emit('mfkip.change'));
        
        const timerId = `openDb:${name}:${Date.now()}`;
        console.time(timerId);
        await db.load();
        console.timeEnd(timerId);

        // On next tick, give an initial change event to kick things off
        requestAnimationFrame(() => db.events.emit('mfkip.change'));
        return new Proxy(db, {
            get(target, prop, receiver) {
                if (DATABASE_MODIFIERS.has(prop)) {
                    return async function() {
                        const result = await Reflect.apply(Reflect.get(target, prop, receiver), this, arguments);
                        db.events.emit('mfkip.change');
                        return result;
                    }
                }

                return Reflect.get(target, prop, receiver);
            }
        });
    }

    async stop() {
        const [ipfs, orbitDb] = await Promise.all([this.ipfs, this.orbitdb]);
        await orbitDb.stop();
        await ipfs.stop();
    }
}

function defered() {
    let resolver;
    let rejecter;
    const promise = new Promise((res, rej) => {
        resolver = res;
        rejecter = rej;
    });

    return { resolver, rejecter, promise };
}

class PubSubService {
    constructor(ipfsPromise) {
        this.ipfsPromise = ipfsPromise;
        this.ipfs = defer(() => ipfsPromise);

        this.localPublish = new Subject();
    }

    on(topic, options) {
        const { echo = true } = options;

        const topic$ = this.ipfs.pipe(
            tap(node => node.libp2p.pubsub.subscribe(topic)),
            finalize(node => node.libp2p.pubsub.unsubscribe(topic)),
            switchMap(node => fromEvent(node.libp2p.pubsub, topic)),
            map(e => JSON.parse(toString(e.data))),
        );

        return echo
            ? merge(topic$, this.localPublish.pipe(
                filter(localEvent => localEvent.topic === topic),
                map(localEvent => localEvent.data)
            ))
            : topic$;
    }

    async publish(topic, data, options) {
        const { echo = true } = options;
        const node = await this.ipfsPromise;
        const jsonData = JSON.stringify(data);

        node.libp2p.pubsub.publish(topic, fromString(jsonData));
        
        if (echo) {
            this.localPublish.next({ topic, data });
        }
    }

}
