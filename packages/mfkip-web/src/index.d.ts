/// <reference path="./orbit-db-types/index.d.ts"       />
import OrbitDB from 'orbit-db';
import { Observable } from 'rxjs';
import type { IPFS } from 'ipfs-core-types';

export interface CreateOptions {
    encodedKey: string,
    listenAddresses: string[],
    appId: string,
    minConnections?: number,
    maxConnections?: number,
    maxData?: number,
    partitions?: string[],
    repo?: string,
}

export interface ListenOptions {
    echo: boolean;
}

export interface PublishOptions {
    echo: boolean;
}

declare class PubSubService {
    on<T = unknown>(topic: string, opts?: ListenOptions): Observable<T>;
    publish(topic: string, serializableData: any, opts?: PublishOptions): Promise<void>;
}

declare global {
    class MFKIP {

        static createSwarmKey(): string;
    
        pubSub: PubSubService;
    
        orbitdb: Promise<OrbitDB>;
        ipfs: Promise<IPFS>;
    
        constructor(options: CreateOptions);
    
        start(): Promise<void>;
        stop(): Promise<void>;

        openDb(name: string, type: TStoreType, options?: ICreateOptions): ReturnType<OrbitDB['create']>;
    }
}
