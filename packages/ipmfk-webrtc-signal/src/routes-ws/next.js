'use strict'

const config = require('../config')
const log = config.log
const socketIO = require('socket.io-next')
const client = require('prom-client')
const { URLSearchParams } = require('url');

const fake = {
  gauge: {
    set: () => { }
  },
  counter: {
    inc: () => { }
  }
}

module.exports = (http, hasMetrics) => {
  const io = socketIO(http.listener, {
    path: '/socket.io-next/' // This should be removed when socket.io@2 support is removed
  })

  http.events.on('stop', () => io.close())

  io.on('connection', handle)

  // Includes all peers across all partitions
  const peers = {}

  // Map partitionId => Set<multiaddr>
  const partitionedPeers = new Map();

  // setInterval(() => {
  //   console.log('Partitions', partitionedPeers);
  // }, 5000);

  const peersMetric = hasMetrics ? new client.Gauge({ name: 'webrtc_star_next_peers', help: 'peers online now' }) : fake.gauge
  const dialsSuccessTotal = hasMetrics ? new client.Counter({ name: 'webrtc_star_next_dials_total_success', help: 'sucessfully completed dials since server started' }) : fake.counter
  const dialsFailureTotal = hasMetrics ? new client.Counter({ name: 'webrtc_star_next_dials_total_failure', help: 'failed dials since server started' }) : fake.counter
  const dialsTotal = hasMetrics ? new client.Counter({ name: 'webrtc_star_next_dials_total', help: 'all dials since server started' }) : fake.counter
  const joinsSuccessTotal = hasMetrics ? new client.Counter({ name: 'webrtc_star_next_joins_total_success', help: 'sucessfully completed joins since server started' }) : fake.counter
  const joinsFailureTotal = hasMetrics ? new client.Counter({ name: 'webrtc_star_next_joins_total_failure', help: 'failed joins since server started' }) : fake.counter
  const joinsTotal = hasMetrics ? new client.Counter({ name: 'webrtc_star_next_joins_total', help: 'all joins since server started' }) : fake.counter

  const refreshMetrics = () => peersMetric.set(Object.keys(peers).length)

  this.io = () => {
    return io
  }

  this.peers = () => {
    return peers
  }

  function safeEmit(addr, event, arg) {
    const peer = peers[addr]
    if (!peer) {
      log('trying to emit %s but peer is gone', event)
      return
    }

    peer.emit(event, arg)
  }

  function handle(socket) {
    const query = new URLSearchParams(socket.request.url.split('?')[1]);
    const partitions = query.has('x-ipmfk-partitions')
      ? query.get('x-ipmfk-partitions').split(',')
      : ['default'];

    socket.on('ss-join', join.bind(socket, partitions))
    socket.on('ss-leave', leave.bind(socket, partitions))
    socket.on('disconnect', disconnect.bind(socket, partitions)) // socket.io own event
    socket.on('ss-handshake', forwardHandshake)
  }

  // join this signaling server network
  function join(myPartitions, multiaddr) {
    joinsTotal.inc()
    if (!multiaddr) { return joinsFailureTotal.inc() }
    const socket = peers[multiaddr] = this // socket

    // Add address to all desired partitions
    console.log('Adding', multiaddr, 'to partions', myPartitions);
    myPartitions.forEach(partition => {
      const peeps = partitionedPeers.get(partition) || new Set();
      peeps.add(multiaddr);
      partitionedPeers.set(partition, peeps);
    });

    let refreshInterval = setInterval(sendPeers, config.refreshPeerListIntervalMS)

    socket.once('ss-leave', stopSendingPeers)
    socket.once('disconnect', stopSendingPeers)

    sendPeers()

    function sendPeers() {
      const peersInMyPartitions = new Set(
        myPartitions
          .flatMap(partition => Array.from(partitionedPeers.get(partition) || new Set()))
      );
      // Dont need to tell myself about myself
      peersInMyPartitions.delete(multiaddr);

      peersInMyPartitions.forEach(mh => {
        safeEmit(mh, 'ws-peer', multiaddr)
      });
    }

    function stopSendingPeers() {
      if (refreshInterval) {
        clearInterval(refreshInterval)
        refreshInterval = null
      }
    }

    joinsSuccessTotal.inc()
    refreshMetrics()
  }

  function leave(partitions, multiaddr) {
    if (!multiaddr) { return }
    if (peers[multiaddr]) {
      delete peers[multiaddr]
      refreshMetrics()
    }

    console.log(`Removing ${multiaddr} from partitions`, partitions);
    partitions.forEach(partition => {
      (partitionedPeers.get(partition) || new Set()).delete(multiaddr);
    });
  }

  function disconnect(partitions) {
    let foundMh;
    Object.keys(peers).forEach((mh) => {
      if (peers[mh].id === this.id) {
        foundMh = mh;
        delete peers[mh]
      }
      refreshMetrics()
    })
    if (!foundMh) return;

    console.log(`Removing ${foundMh} from partitions`, partitions);
    partitions.forEach(partition => {
      (partitionedPeers.get(partition) || new Set()).delete(foundMh);
    });
  }

  // forward an WebRTC offer to another peer
  function forwardHandshake(offer) {
    dialsTotal.inc()
    if (offer == null || typeof offer !== 'object' || !offer.srcMultiaddr || !offer.dstMultiaddr) { return dialsFailureTotal.inc() }

    const dstPartitions = Array.from(partitionedPeers.entries())
      .filter(([partitionId, peerAddrs]) => peerAddrs.has(offer.dstMultiaddr))
      .map(([partitionId]) => partitionId);

    const srcPartitions = Array.from(partitionedPeers.entries())
      .filter(([partitionId, peerAddrs]) => peerAddrs.has(offer.srcMultiaddr))
      .map(([partitionId]) => partitionId);

    const hasCommonPartition = srcPartitions.some(partition => dstPartitions.includes(partition));

    if (!hasCommonPartition) {
      dialsFailureTotal.inc()
      offer.err = 'peer not in your partitions'
      safeEmit(offer.srcMultiaddr, 'ws-handshake', offer)

      return;
    }

    if (offer.answer) {
      dialsSuccessTotal.inc()
      safeEmit(offer.srcMultiaddr, 'ws-handshake', offer)
    } else {
      if (peers[offer.dstMultiaddr]) {
        safeEmit(offer.dstMultiaddr, 'ws-handshake', offer)
      } else {
        dialsFailureTotal.inc()
        offer.err = 'peer is not available'
        safeEmit(offer.srcMultiaddr, 'ws-handshake', offer)
      }
    }
  }

  return this
}
