import { start } from 'ipmfk-webrtc-signal';

export function startSignalingServer() {
    return start();
}