import { start } from "our-libp2p-core";
import * as Protector from 'libp2p/src/pnet';
import * as wrtc from 'wrtc';
import { from, fromEvent, Subject } from "rxjs";
import { debounceTime, startWith, switchMap } from 'rxjs/operators';
import OrbitDB from 'orbit-db';
import { Config } from './args';
import { startSignalingServer } from './signal-server';

let swarmKey: Uint8Array = Config.key
    ? new Uint8Array(Buffer.from(Config.key, 'base64'))
    : new Uint8Array(95);

if (!(Config.key)) {
    Protector.generate(swarmKey);
}

console.log('SwarmKey: ', Buffer.from(swarmKey).toString('base64'));

const chatDbOptions = {
    accessController: {
        write: ['*']
    }
};

(async () => {

    if (Config.signalServer) {
        await startSignalingServer();
    }

    const node = await start({
        encodedKey: Buffer.from(swarmKey).toString('base64'),
        listenAddresses: ['/ip4/127.0.0.1/tcp/13579/wss/p2p-webrtc-star'],
        wrtc
    });

    setInterval(() => {
        const peers = (node as any).libp2p.metrics.peers;
        console.log(`peers: ${peers.length}`);
    }, 5000);

    const orbitdb = await OrbitDB.createInstance(node);
    const chatUpdater = new Subject();

    const store = orbitdb.log("chat.v5", chatDbOptions)
        .then(async (store: any) => {
            console.time('loading');
            await store.load();
            console.timeEnd('loading');

            // console.log(store);

            return store;
        });

    chatUpdater.pipe(
        debounceTime(100),
    ).subscribe(async () => {
        const myDb = await store;
        const chatLog = myDb.iterator({ limit: -1 }).collect();

        console.log(
            chatLog
                .map((e: any) => e.payload.value)
                .sort((c1: any, c2: any) => c1.createdAt - c2.createdAt)
        );
    });

    from(store).pipe(
        switchMap((store: any) => fromEvent(store.events, 'replicated')),
        startWith(null),
    ).subscribe(() => {
        chatUpdater.next(null);
    });
})();
