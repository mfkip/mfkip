import parseArgs from 'minimist';

export const Config = parseArgs(process.argv, {
    boolean: [
        'signal-server'
    ],
    string: [
        'key'
    ],
    alias: {
        'key': 'k',
        'signal-server': 'signalServer'
    },
    default: {
        'signal-server': true,
    }
});

console.log('Starting with Config:', Config);
