import * as OrbitDB from 'orbit-db'; 
import { libp2pNode } from 'our-libp2p-core';

export { start, libp2pNode } from 'our-libp2p-core';

// export async function start({
//     encodedKey, listenAddresses, minConnections, maxConnections, maxData
// }) {
//     const swarmKey = uint8FromString(atob(encodedKey));
//     const libP2pConfig = (opts) => new Libp2p({
//         peerId: opts.peerId,
//         modules: {
//             transport: [WebRTCStar],
//             streamMuxer: [MPLEX],
//             connEncryption: [NOISE],
//             connProtector: new Protector(swarmKey),
//             pubsub: gossipsub,
//         },
//         connectionManager: {
//             minConnections,
//             maxConnections,
//             maxData,
//         },
//         addresses: {
//             listen: listenAddresses
//         },
//         metrics: {
//             enabled: true,
//             computeThrottleMaxQueueSize: 1000,
//             computeThrottleTimeout: 2000,
//             movingAverageIntervals: [
//                 60 * 1000,
//                 5 * 60 * 1000,
//                 15 * 60 * 1000 // 15 minutes
//             ],
//             maxOldPeersRetention: 50
//         }
//     });

//     const node = await IPFSCreate({
//         start: false,
//         preload: { enabled: false },
//         libp2p: libP2pConfig
//     });

//     console.log('Starting');
//     await node.start();
//     console.log('Started');

//     node.libp2p.connectionManager.on('peer:connect', (conn) => {
//         console.log('>>>>> Connected', conn.remotePeer.toB58String());
//     })
//     node.libp2p.connectionManager.on('peer:disconnect', (conn) => {
//         console.log('>>>>> Disconnected', conn.remotePeer.toB58String());
//     })

//     window.ipfs = node;

//     libp2pNodeResolver(node);
//     return node;
// };

export const orbitDbNode = (async () => {
    return await OrbitDB.createInstance(await libp2pNode);
})();
