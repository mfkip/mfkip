import { libp2pNode, orbitDbNode } from './libp2p';
import { Store } from 'svelte/store';
import { from, fromEvent, Subject } from 'rxjs';
import { debounceTime, startWith, switchMap } from 'rxjs/operators';

const chatDbOptions = {
    accessController: {
        write: ['*']
    }
}

export const ChatStore = window.ChatStore = new (class _ChatStore extends Store {


    constructor() {
        super({ chats: [], byRoom: new Map() });
        this.chatUpdater = new Subject();

        this.db = orbitDbNode
            .then(node => node.log("chat.v5", chatDbOptions))
            .then(async db => {
                console.time('loading');
                await db.load();
                console.timeEnd('loading');

                console.log(db);
                
                return db;
            });

        this.chatUpdater.pipe(
            debounceTime(100),
        ).subscribe(async () => {
            const db = await this.db;
            const chatLog = db.iterator({ limit: -1 }).collect();

            this.set({ 
                chats: chatLog
                    .map(e => e.payload.value)
                    .sort((c1, c2) => c1.createdAt - c2.createdAt)
            });
        });

        from(this.db).pipe(
            switchMap(db => fromEvent(db.events, 'replicated')),
            startWith(null),
        ).subscribe(() => {
            this.updateChats();
        });

        this.compute('byRoom', ['chats'], (chats) => {
            return chats.reduce((acc, chat) => {
                const chats = acc.get(chat.roomId) || [];
                chats.push(chat);

                return acc.set(chat.roomId, chats);
            }, new Map());
        });
    }

    async sendChat(msg, roomId) {
        const db = await this.db;
        await db.add({ msg, roomId, createdAt: Date.now() });
        this.updateChats();
    }

    async updateChats() {
        this.chatUpdater.next(null);
    }

    createRoom(roomId) {
        return this.sendChat('[Room Created]', roomId);
    }
})();