import { libp2pNode } from 'our-libp2p-core';
import { AppStore } from './App.store';
import { UserStore } from './user.store';
import * as PeerId from 'peer-id';

setInterval(async function () {
    const libp2p = (await libp2pNode).libp2p;
    const peers = await Promise.all(libp2p.metrics.peers
        .map(async peer => {
            try {

                const peerId = PeerId.createFromB58String(peer);
                return {
                    peerId: peer,
                    name: await UserStore.identifyPeer(peer),
                    latency: await libp2p.ping(peerId),
                    stats: libp2p.metrics.forPeer(peerId),
                }
            } catch(e) {
                return {
                    peerId: peer,
                    name: 'waiting for peer...',
                    latency: 0
                }
            }
        }));

    AppStore.set({
        metrics: {
            global: libp2p.metrics.global,
            peers: peers,
        },
    })
}, 1000);