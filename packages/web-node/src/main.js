import App from './App.html';
import { AppStore } from './App.store';

const app = new App({
	target: document.body,
});

// function checkForSwSupport() {
//     if (!('serviceWorker' in navigator)) {
//         throw new Error('No Service Worker support!')
//     }
// }

// async function registerServiceWorker() {
//     return await navigator.serviceWorker.register('./bundle.service.js', { scope: './' });
// }

// if (!window.dev) {
// 	(async () => {
// 		checkForSwSupport();
// 		await registerServiceWorker();
// 	})()
// } else {
// 	console.log('Detected Dev Mode');
// }

window.addEventListener('beforeinstallprompt', e => {
	AppStore.set({ installPrompt: e });
});

window.addEventListener('appinstalled', e => {
	console.log('window.appinstalled', e);
});

export default app;