import { orbitDbNode, libp2pNode } from './libp2p';
import { Store } from 'svelte/store';

const userDbOptions = {
    accessController: {
        write: ['*']
    }
}

export const UserStore = new (class _UserStore extends Store {
    constructor() {
        super();
        this.db = orbitDbNode
            .then(node => node.keyvalue('user', userDbOptions))
            .then(async db => {
                await db.load();
                return db;
            });
    }

    async setUserName(username) {
        const db = await this.db;
        const libp2p = (await libp2pNode).libp2p;
        return db.put(libp2p.peerId.toB58String(), { name: username });
    }

    async identifyPeer(peerId) {
        const db = await this.db;
        const user = await db.get(peerId);
        if (user) {
            return user.name;
        }

        return '';
    }

    async whoAmI() {
        const libp2p = (await libp2pNode).libp2p;
        
        return this.identifyPeer(libp2p.peerId.toB58String());
    }
})();